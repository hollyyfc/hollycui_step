[![pipeline status](https://gitlab.com/hollyyfc/hollycui_step/badges/main/pipeline.svg)](https://gitlab.com/hollyyfc/hollycui_step/-/commits/main)

# 🔮 Holly's Rust AWS Lambda with Step Functions

<blockquote>
<i>I know I overcomplicated my zodiac obsession, but hey, it's all in good fun!</i>
</blockquote>

## Description

This project implements [Rust](https://www.rust-lang.org/), [AWS Lambda](https://aws.amazon.com/lambda/), and [Step Functions](https://aws.amazon.com/step-functions/) to orchestrate a zodiac checkup workflow. It consists of three Rust-based Lambda functions: 

1. [zodiac-sign](zodiac_sign): This is the starting point of our workflow which determines a user's **zodiac sign** based on their **birthday** input (*MM/DD*)
2. [triplicity](triplicity): As a subsequent function, it identifies the **triplicity** (*Fire, Earth, Air, Water*) associated with the previous **zodiac sign** output
3. [compatibility](compatibility): The last function returns **compatible zodiac signs** within a group of triplicity and their **traits** based on the determined **triplicity** 

These functions are individually created in their own directory, but inherently connected with each other through the definition of input and output using Rust. The final realization is through a linearly defined AWS Step Functions state machine, ensuring smooth data flow and task orchestration. GitLab CI/CD pipeline has also been enabled to streamline the process of function deployment and speedy updates. 

## Demo

### Video

Click below and get redirected to YouTube for watching: 

[![Project Demo](http://img.youtube.com/vi/xR1-UmEhu1A/0.jpg)](http://www.youtube.com/watch?v=xR1-UmEhu1A)

### Highlights

- **Lambda Functions**

![zodiac](https://gitlab.com/hollyyfc/hollycui_step/-/wikis/uploads/04d38ca58b0c495093396eb3f89ee03a/zodiac.png)

![trip](https://gitlab.com/hollyyfc/hollycui_step/-/wikis/uploads/6694e6ef7b1e9e7acfd5a28b1457671c/trip.png)

![comp](https://gitlab.com/hollyyfc/hollycui_step/-/wikis/uploads/b9e773e962a32829d91849a778e6b264/comp.png)

- **Step Function: State Machine**

![state](https://gitlab.com/hollyyfc/hollycui_step/-/wikis/uploads/eb4cd9dc8815837882bf1969c52ad414/state.png)

![graph](https://gitlab.com/hollyyfc/hollycui_step/-/wikis/uploads/da8f7bf7fc61cdb710fb1733de93240c/graph.png)

- **Test Pipeline**

![output](https://gitlab.com/hollyyfc/hollycui_step/-/wikis/uploads/5872a28a8128b9351e11398f2e85481f/output.png)

## Steps Walkthrough

- Create a general directory housing the project
- Create the function subdirectorie(s) individually by `cargo lambda new <function_name>` inside the general directory
- Add dependencies by `cargo add <crate_name> --features "<feature_name>"`
- Build the functions in order:
    - `cd <function_name>`
    - The functions should be designed with connected input and output:
    ```rust
    #[derive(Deserialize, Serialize)]
    struct Input {
        previous_output: String,
    }
    struct Output {
        next_input: String,
    }
    ```
    - Though connected, you should still be able to test run each single function by itself
- Run `cargo lambda watch` and `cargo lambda invoke` to test each function

---

- <ins>AWS IAM</ins>: Create a new IAM user with the follwing policies attached
    - `IAMFullAccess`
    - `AWSLambda_FullAccess`
- Push all functions to a newly created GitLab repo
- Save the new IAM's `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` as GitLab CI/CD variables
- Write a [`.gitlab-ci.yml`](.gitlab-ci.yml) file for auto-deployment from GitLab to AWS Lambda
    - Each function should be deployed individually. Use stages for separate deployment!
- <ins>AWS Lambda</ins>: Check whether the functions are successfully deployed

---

- <ins>AWS Step Functions</ins>: Create a new state machine
    - Drag the `AWS Lambda Invoke` block to the graph & choose a function
    - Organize the order of the functions
    - Specify details and descriptions if needed
- Check the newly created machine & perform some test cases
    - Can directly invoke on AWS
    - Structure the input of the first function as JSON (this is the only input we need for the workflow)
    - Check each state and logs for interim output
