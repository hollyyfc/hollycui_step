use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Deserialize, Serialize)]
struct Input {
    birthday: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    zodiac_sign: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(find_zodiac_sign);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn find_zodiac_sign(event: LambdaEvent<Input>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    let zodiac_sign = get_zodiac_sign(&event.birthday);
    Ok(serde_json::json!({ "zodiac_sign": zodiac_sign }))
}

fn get_zodiac_sign(birthday: &str) -> String {
    let date_parts: Vec<&str> = birthday.split('/').collect();
    if date_parts.len() != 2 {
        return "Invalid date".to_string();
    }

    let month: i32 = date_parts[0].parse().unwrap_or(0);
    let day: i32 = date_parts[1].parse().unwrap_or(0);

    match (month, day) {
        (3, 21..=31) | (4, 1..=19) => "Aries",
        (4, 20..=30) | (5, 1..=20) => "Taurus",
        (5, 21..=31) | (6, 1..=20) => "Gemini",
        (6, 21..=30) | (7, 1..=22) => "Cancer",
        (7, 23..=31) | (8, 1..=22) => "Leo",
        (8, 23..=31) | (9, 1..=22) => "Virgo",
        (9, 23..=30) | (10, 1..=22) => "Libra",
        (10, 23..=31) | (11, 1..=21) => "Scorpio",
        (11, 22..=30) | (12, 1..=21) => "Sagittarius",
        (12, 22..=31) | (1, 1..=19) => "Capricorn",
        (1, 20..=31) | (2, 1..=18) => "Aquarius",
        (2, 19..=28) | (3, 1..=20) => "Pisces",
        _ => "Unknown",
    }.to_string()
}

