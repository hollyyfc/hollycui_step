use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Deserialize, Serialize)]
struct Input {
    zodiac_sign: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    triplicity: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(find_triplicity);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn find_triplicity(event: LambdaEvent<Input>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    let triplicity = get_triplicity(&event.zodiac_sign);
    Ok(serde_json::json!({ "triplicity": triplicity }))
}

fn get_triplicity(zodiac_sign: &str) -> String {
    match zodiac_sign {
        "Aries" | "Leo" | "Sagittarius" => "Fire",
        "Taurus" | "Virgo" | "Capricorn" => "Earth",
        "Gemini" | "Libra" | "Aquarius" => "Air",
        "Cancer" | "Scorpio" | "Pisces" => "Water",
        _ => "Unknown",
    }.to_string()
}
