use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::{Value, json};

#[derive(Deserialize, Serialize)]
struct Input {
    triplicity: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    compatible_zodiacs: Value,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(find_compatible_zodiacs);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn find_compatible_zodiacs(event: LambdaEvent<Input>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    let response = match event.triplicity.as_str() {
        "Fire" => json!({
            "zodiacs": ["Aries", "Leo", "Sagittarius"],
            "traits": "energetic, impulsive, confident, motivated"
        }),
        "Earth" => json!({
            "zodiacs": ["Taurus", "Virgo", "Capricorn"],
            "traits": "practical, realistic, patient, dependable, thorough"
        }),
        "Air" => json!({
            "zodiacs": ["Gemini", "Libra", "Aquarius"],
            "traits": "intellectual, objective, talkative, indecisive, observant"
        }),
        "Water" => json!({
            "zodiacs": ["Cancer", "Scorpio", "Pisces"],
            "traits": "moody, sensitive, secretive, intuitive, vulnerable"
        }),
        _ => json!({
            "error": "Unknown triplicity"
        })
    };
    Ok(serde_json::json!({ "compatible_zodiacs": response }))
}
